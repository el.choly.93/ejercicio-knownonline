# Ejercicio KnowOnline de Ariel Aguirre
## instrucciones para instalar y correr
- Una vez descargado el proyecto moverse a la carpeta del proyecto
- Agregar a la carpeta del proyecto, el archivo .env enviado por otro medio.
- Editar el archivo .env con las credenciales correctas para conectarse a la base de datos local (incluyendo nombre de una base de datos mysql recien creada)
- Correr composer para instalar todas las dependencias -> composer install
- Correr las migrations, para que se generen las tablas necesarias en la base de datos -> php artisan migrate
- Ya estamos en condiciones de correr el comando desarrollado -> php artisan command:miracionOrdenesCompraFinalizada

## ¿Que ocurre si hay errores?
- Cualquier error de migracion se escribe al archivo de log de laravel, presente en storage/logs/laravel.log

## Quedo a disposición
- Me mantengo a disposición por cualquier consulta