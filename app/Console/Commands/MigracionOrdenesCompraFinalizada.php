<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\MigracionOrdenesCompra;

class MigracionOrdenesCompraFinalizada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:miracionOrdenesCompraFinalizada';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migracion de Ordenes de Compra Finalizadas desde principio de año';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // try y catch
        $migracionOrdenesCompra = new MigracionOrdenesCompra();
        $migracionOrdenesCompra->__invoke();
    }
}
