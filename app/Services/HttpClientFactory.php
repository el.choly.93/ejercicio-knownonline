<?php

namespace App\Services;

use GuzzleHttp\Client;

/*
 * Fabrica para centralizar la creación del cliente http
*/
class HttpClientFactory {

    public function createClient() {
        $headers = [
            'x-vtex-api-appkey' => env('VTEX_API_APPKEY'),
            'X-VTEX-API-AppToken' => env('VTEX_API_APPTOKEN')
        ];
        $client = new Client([
            'headers' => $headers,
            'base_uri' => env('BASE_URL_VTEX'),
            'timeout'  => 60.0,
        ]);
        return $client;
    }

}