<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

class MigracionOrdenesCompra {

    private $ordenesCompraService;

    public function __construct() {
        $this->ordenesCompraService = new OrdenesCompraService();
    }

    public function __invoke() {
        $httpClientFactory = new HttpClientFactory();
        $client = $httpClientFactory->createClient();

        $fechaDesde = new Carbon('first day of January');
        $fechaDesde->startOfDay();
        $fechaDesde = $fechaDesde->format('Y-m-d\TH:i:s.v\Z');

        $fechaHasta = Carbon::now('UTC');
        $fechaHasta = $fechaHasta->format('Y-m-d\TH:i:s.v\Z');

        DB::beginTransaction(); // unica transaccion para toda la migracion
        try {
            $pagina = 0;
            do {
                $pagina++;
                $queryParametros = [
                    'f_creationDate' => 'creationDate:['.$fechaDesde.' TO '.$fechaHasta.']',
                    'f_status' => 'invoiced',
                    'per_page' => 100,
                    'page' => $pagina
                ];
                $responseOrders = $client->get('oms/pvt/orders', ['query' => $queryParametros]);
                $bodyResponseOrders = json_decode( (string) $responseOrders->getBody(), true );
                $listaOrdenes = $bodyResponseOrders['list'];
                foreach ( $listaOrdenes as $orden ) {
                    $responseDetalleOrden = $client->get('oms/pvt/orders/'.$orden['orderId']); // obtengo detalle de la order
                    $bodyResponseDetalleOrden = json_decode( (string) $responseDetalleOrden->getBody(), true );
                    // envio las 2 entidades migradas al servicio que se encarga de guardar la informacion necesaria
                    $this->ordenesCompraService->almacenarOrdenCompraItems( $orden, $bodyResponseDetalleOrden );
                }
            } while ( $pagina < $bodyResponseOrders['paging']['pages'] );
        } catch ( \Exception $e) {
            DB::rollBack(); // ante cualquier error se anula toda la transaccion
            Log::error( $e->getMessage() );
        }
        DB::commit();
    }

}