<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;

use App\Models\Orden;
use App\Models\Item;

class OrdenesCompraService {

    public function almacenarOrdenCompraItems( $orden, $detalleOrden ) {
        $ordenLocal = Orden::where('order_id', '=', $orden['orderId'])
            ->first();
        if ( $ordenLocal == null ) {
            $ordenLocal = new Orden();
        }
        $ordenLocal->order_id = $orden['orderId'];
        $cliente = "";
        $cliente .= $detalleOrden['clientProfileData']['firstName'].' '; // esto va a cliente
        $cliente .= $detalleOrden['clientProfileData']['lastName'].' ';
        $cliente .= $detalleOrden['clientProfileData']['email'];
        $ordenLocal->cliente = $cliente;
        $ordenLocal->medio_pago_id = $detalleOrden['paymentData']['transactions'][0]['payments'][0]['id'];
        $ordenLocal->monto_total = $orden['totalValue'];
        $ordenLocal->save();

        $listaItems = $detalleOrden['items'];
        foreach($listaItems as $item) {
            $itemLocal = Item::where('order_id', '=', $orden['orderId'])
                ->where('ref_id', '=', $item['refId'])
                ->where('product_id', '=', $item['productId'])
                ->first();
            if ( $itemLocal == null ) {
                $itemLocal = new Item();
            }
            $itemLocal->order_id = $orden['orderId'];
            $itemLocal->ref_id = $item['refId'];
            $itemLocal->product_id = $item['productId'];
            $itemLocal->quantity = $item['quantity'];
            $itemLocal->name = $item['name'];
            $itemLocal->save();
        }
    }

}