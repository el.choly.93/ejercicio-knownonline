<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->id(); // tiene su propia clave primaria, para evitar inconvenientes con valores de migraciones

            $table->integer('product_id');
            $table->string('ref_id'); // varia aunque sea el mismo product id
            $table->string('name');
            $table->integer('quantity');

            $table->string('order_id');
            $table->foreign('order_id')->references('order_id')->on('orden');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
