<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden', function (Blueprint $table) {
            $table->string('order_id');
            $table->string('cliente');
            $table->string('medio_pago_id');
            $table->float('monto_total', 10, 2); // rango hasta 99 millones con 2 decimales => 99.999.999,99

            $table->primary('order_id'); // defino clave primaria
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden');
    }
}
